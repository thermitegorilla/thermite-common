﻿namespace Thermite.Common
{
   using System;
   using System.ComponentModel;

   /// <summary>
   /// A generic wrapper for objects that allows editing and reseting of property values.
   /// </summary>
   /// <typeparam name="T">The type of the object to be wrapped.</typeparam>
   public class GenericEditableObject<T> : MockTypeDescriptor<T>, IEditableObject, INotifyPropertyChanged
   {
      /// <summary>
      /// Handles property changes on the underlying object.
      /// </summary>
      private event PropertyChangedEventHandler PropertyChanged;

      /// <summary>
      /// Property change handler that links the property change handler in this wrapper with 
      /// the handler, if any, in the wrapped object.
      /// </summary>
      event PropertyChangedEventHandler INotifyPropertyChanged.PropertyChanged
      {
         add
         {
            this.PropertyChanged += value;
            if (SourceObject is INotifyPropertyChanged)
            {
               ((INotifyPropertyChanged)SourceObject).PropertyChanged += OnNotifyPropertyChanged;
            }
         }

         remove
         {
            this.PropertyChanged -= value;
            if (SourceObject is INotifyPropertyChanged)
            {
               ((INotifyPropertyChanged)SourceObject).PropertyChanged -= OnNotifyPropertyChanged;
            }
         }
      }

      /// <summary>
      /// A store for storing the original property values of the wrapped object.
      /// </summary>
      private PropertyValueBackup<T> backup;

      /// <summary>
      /// Initializes a new instance of the <see cref="GenericEditableObject{T}"/> class.
      /// </summary>
      /// <param name="obj">Object to wrap.</param>
      public GenericEditableObject(T obj) : base(obj) { }

      /// <summary>
      /// Start editing of the wrapped object.
      /// </summary>
      public void BeginEdit() => backup = GetBackup();

      /// <summary>
      /// Cancel the editing, resetting any changes made to the wrapped object.
      /// </summary>
      public void CancelEdit()
      {
         RestoreBackup();
         DeleteBackup();
      }

      /// <summary>
      /// End the editing, saving any changes made to the wrapped object.
      /// </summary>
      public void EndEdit() => DeleteBackup();

      /// <summary>
      /// Creates or retrieves the backup of the wrapped object.
      /// </summary>
      /// <returns>The <see cref="PropertyValueBackup{T}"/> representing the state of the 
      /// wrapped object.</returns>
      private PropertyValueBackup<T> GetBackup() => backup ?? new PropertyValueBackup<T>(SourceObject);

      /// <summary>
      /// Restore the backup to the wrapped object.
      /// </summary>
      private void RestoreBackup() => backup?.Restore(SourceObject);

      /// <summary>
      /// Deletes the backup of the wrapped object.
      /// </summary>
      private void DeleteBackup() => backup = null;

      /// <summary>
      /// Invokes the property changed handler.
      /// </summary>
      /// <param name="sender">The sender of the event.</param>
      /// <param name="e">The <see cref="PropertyChangedEventArgs"/> to send.</param>
      private void OnNotifyPropertyChanged(object sender, PropertyChangedEventArgs e) => PropertyChanged?.Invoke(this, e);
   }
}
