﻿

namespace Thermite.Common
{
   using System;
   using System.Collections.Generic;
   using System.Linq;
   using System.Reflection;

   /// <summary>
   /// 
   /// </summary>
   /// <typeparam name="T"></typeparam>
   public class PropertyValueBackup<T>
   {
      /// <summary>
      /// 
      /// </summary>
      private IDictionary<PropertyInfo, object> propertyValues = new Dictionary<PropertyInfo, object>();

      /// <summary>
      /// 
      /// </summary>
      /// <param name="obj"></param>
      public PropertyValueBackup(T obj)
      {
         var properties = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance)
            .Where(pi => pi.CanRead && pi.CanWrite);

         foreach (var pi in properties)
         {
            propertyValues[pi] = pi.GetValue(obj);
         }
      }

      /// <summary>
      /// 
      /// </summary>
      /// <param name="obj"></param>
      public void Restore(T obj)
      {
         foreach (var pair in propertyValues)
         {
            pair.Key.SetValue(obj, pair.Value);
         }
      }
   }
}
