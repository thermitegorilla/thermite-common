﻿namespace Thermite.Common
{
   using System;
   using System.ComponentModel;

   /// <summary>
   /// A custom type descriptor which acts as though it is for the type <see cref="T"/>.
   /// </summary>
   /// <typeparam name="T">The type to return the descriptor of.</typeparam>
   public class MockTypeDescriptor<T> : ICustomTypeDescriptor
   {
      /// <summary>
      /// Object that is being wrapped and described.
      /// </summary>
      protected T SourceObject { get; }

      /// <summary>
      /// Initializes a new instance of the <see cref="MockTypeDescriptor{T}"/> class.
      /// </summary>
      /// <param name="obj">Object to wrap and describe.</param>
      public MockTypeDescriptor(T obj)
      {
         SourceObject = obj;
      }

      /// <summary>
      /// Returns a collection of custom attributes for this instance of a component.
      /// </summary>
      /// <returns>An <see cref="AttributeCollection"/> containing the attributes 
      /// for the <see cref="SourceObject"/>.</returns>
      public AttributeCollection GetAttributes() => TypeDescriptor.GetAttributes(SourceObject);

      /// <summary>
      /// Returns the class name of the <see cref="SourceObject"/>.
      /// </summary>
      /// <returns>The class name of the <see cref="SourceObject"/>, or null if the class does not 
      /// have a name.</returns>
      public string GetClassName() => TypeDescriptor.GetClassName(SourceObject);

      /// <summary>
      /// Returns the name of the <see cref="SourceObject"/>.
      /// </summary>
      /// <returns>The name of the <see cref="SourceObject"/>, or null if the object does not have 
      /// a name.</returns>
      public string GetComponentName() => TypeDescriptor.GetComponentName(SourceObject);

      /// <summary>
      /// Returns a type converter for the <see cref="SourceObject"/>.
      /// </summary>
      /// <returns>A <see cref="TypeConverter"/> that is the converter the <see cref="SourceObject"/>,
      /// or null if there is no <see cref="TypeConverter"/> for the <see cref="SourceObject"/>.</returns>
      public TypeConverter GetConverter() => TypeDescriptor.GetConverter(SourceObject);

      /// <summary>
      /// Returns the default event for the <see cref="SourceObject"/>.
      /// </summary>
      /// <returns>An <see cref="EventDescriptor"/> that represents the default event for
      /// the <see cref="SourceObject"/>, or null if the <see cref="SourceObject"/> does not 
      /// have events.</returns>
      public EventDescriptor GetDefaultEvent() => TypeDescriptor.GetDefaultEvent(SourceObject);

      /// <summary>
      /// Returns the default property for the <see cref="SourceObject"/>.
      /// </summary>
      /// <returns>A <see cref="PropertyDescriptor"/> that represents the default property
      /// for the <see cref="SourceObject"/>, or null if the <see cref="SourceObject"/> does not 
      /// have properties.</returns>
      public PropertyDescriptor GetDefaultProperty() => TypeDescriptor.GetDefaultProperty(SourceObject);

      /// <summary>
      /// Returns an editor of the specified type for the <see cref="SourceObject"/>.
      /// </summary>
      /// <param name="editorBaseType">A <see cref="Type"/> that represents the editor for 
      /// the <see cref="SourceObject"/>.</param>
      /// <returns>An <see cref="object"/> of the specified type that is the editor for the <see cref="SourceObject"/>, or
      /// null if the editor cannot be found.</returns>
      public object GetEditor(Type editorBaseType) => TypeDescriptor.GetEditor(SourceObject, editorBaseType);

      /// <summary>
      /// Returns the events for the <see cref="SourceObject"/>.
      /// </summary>
      /// <returns>An <see cref="EventDescriptorCollection"/> that represents the events
      /// for the <see cref="SourceObject"/>.</returns>
      public EventDescriptorCollection GetEvents() => TypeDescriptor.GetEvents(SourceObject);

      /// <summary>
      /// Returns the events for the <see cref="SourceObject"/> using the specified attribute
      /// array as a filter.
      /// </summary>
      /// <param name="attributes">An array of type <see cref="Attribute"/> that is used as a filter.</param>
      /// <returns>An <see cref="EventDescriptorCollection"/> that represents the filtered
      /// events for the <see cref="SourceObject"/>.</returns>
      public EventDescriptorCollection GetEvents(Attribute[] attributes) => TypeDescriptor.GetEvents(SourceObject, attributes);

      /// <summary>
      /// Returns the properties for the <see cref="SourceObject"/>.
      /// </summary>
      /// <returns>A <see cref="PropertyDescriptorCollection"/> that represents the properties
      /// for the <see cref="SourceObject"/>.</returns>
      public PropertyDescriptorCollection GetProperties() => TypeDescriptor.GetProperties(SourceObject);

      /// <summary>
      /// Returns the properties for the <see cref="SourceObject"/> using the attribute array as a filter.
      /// </summary>
      /// <param name="attributes">An array of type <see cref="Attribute"/> that is used as a filter.</param>
      /// <returns>A <see cref="PropertyDescriptorCollection"/> that represents the filtered
      /// properties for the <see cref="SourceObject"/>.</returns>
      public PropertyDescriptorCollection GetProperties(Attribute[] attributes) => TypeDescriptor.GetProperties(SourceObject, attributes);

      /// <summary>
      /// Returns an object that contains the property described by the specified property descriptor.
      /// </summary>
      /// <param name="pd">A <see cref="PropertyDescriptor"/> that represents the property whose
      /// owner is to be found.</param>
      /// <returns>An <see cref="object"/> that represents the owner of the specified property.</returns>
      public object GetPropertyOwner(PropertyDescriptor pd) => SourceObject;
   }
}
