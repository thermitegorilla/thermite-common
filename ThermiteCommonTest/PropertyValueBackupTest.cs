﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ThermiteCommonTest
{
   /// <summary>
   /// Tests for the PropertyValueBackup Class
   /// Documentation: 
   /// </summary>
   [TestFixture(Description = "Tests for PropertyValueBackup")]
   class PropertyValueBackupTest
   {
      #region Class Variables
      #endregion

      #region Setup/Teardown

      /// <summary>
      /// Code that is run once at start of test suite
      /// </summary>
      [OneTimeSetUp]
      public void TestFixtureSetup()
      {
      }

      /// <summary>
      /// Code that is run once at end of test suite
      /// </summary>
      [OneTimeTearDown]
      public void TestFixtureTearDown()
      {
      }

      /// <summary>
      /// Code that is run before each test
      /// </summary>
      [SetUp]
      public void Initialize()
      {
      }

      /// <summary>
      /// Code that is run after each test
      /// </summary>
      [TearDown]
      public void Cleanup()
      {
      }

      #endregion

      #region Property Tests
      #endregion

      #region Constructor Tests

      /// <summary>
      /// PropertyValueBackup Constructor Test
      /// Documentation        :  
      /// Constructor Signature:  PropertyValueBackup<T>(T obj)
      /// </summary>
      [Test]
      public void PropertyValueBackupConstructorTest()
      {
         throw new NotImplementedException("Test not yet implemented");
      }

      #endregion

      #region Method Tests
      #endregion
   }
}
